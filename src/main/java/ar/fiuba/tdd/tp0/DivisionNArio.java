package ar.fiuba.tdd.tp0;

public class DivisionNArio implements Content, Function {

    public float operate(Operator op1, Operator op2) {
        return 0;
    }

    public float operate(Operator op1, Operator op2, Operator op3, Operator op4) {
        return (((op4.getValue() / op3.getValue()) / op2.getValue()) / op1.getValue());
    }

    public float operate(Operator op1) {
        return op1.getValue();
    }

}