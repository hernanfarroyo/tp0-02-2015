package ar.fiuba.tdd.tp0;

public interface Factory<T> {
    public T fabricate(String name);
    
}