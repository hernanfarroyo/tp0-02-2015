package ar.fiuba.tdd.tp0;

public interface Function {
    public float operate(Operator op1, Operator op2);
    
    public float operate(Operator op1, Operator op2, Operator op3, Operator op4);
    
    public float operate(Operator op1);
}