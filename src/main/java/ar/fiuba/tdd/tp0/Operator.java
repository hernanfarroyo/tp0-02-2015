package ar.fiuba.tdd.tp0;

public class Operator implements Content{

    private float value;

    public Operator(float value) {
        this.setValue(value);
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

}