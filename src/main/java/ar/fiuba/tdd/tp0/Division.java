package ar.fiuba.tdd.tp0;

public class Division implements Content, Function {

    public float operate(Operator op1, Operator op2) {
        return (op2.getValue() / op1.getValue());
    }

    public float operate(Operator op1, Operator op2, Operator op3, Operator op4) {
        return 0;
    }

    public float operate(Operator op1) {
        throw new IllegalArgumentException();
    }

}