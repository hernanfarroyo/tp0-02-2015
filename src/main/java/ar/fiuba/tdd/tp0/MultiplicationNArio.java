package ar.fiuba.tdd.tp0;

public class MultiplicationNArio implements Content, Function {

    public float operate(Operator op1, Operator op2) {
        return 0;
    }

    public float operate(Operator op1, Operator op2, Operator op3, Operator op4) {
        float valor = (op1.getValue() * op2.getValue() * op3.getValue() * op4.getValue());
        return valor;
    }

    public float operate(Operator op1) {
        return op1.getValue();
    }

}