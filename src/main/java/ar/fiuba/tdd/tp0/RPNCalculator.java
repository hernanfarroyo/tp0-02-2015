package ar.fiuba.tdd.tp0;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class RPNCalculator {
    private static final String OPERATORNAME = "Operator";
    static String NULL = null;
    private Parser parser = new Parser();
    private Queue<Content> contents = new LinkedList<Content>();
    private Stack<Content> stack = new Stack<Content>();

    private void operateWith(Stack<Content> stack2) {
        Operator op1 = null;
        Operator op2 = null;
        Operator op3 = null;
        Operator op4 = null;
        float value1Operator = 0;
        float value2Operator = 0;
        float value4Operator = 0;
        Function function;
        
        while (stack2.size() == 1) {
            throw new IllegalArgumentException();
        }

        function = (Function) stack2.pop();
        op1 = (Operator) stack2.pop();
        boolean comeIn = true;
        boolean existOneOperator = stack2.size() == 0;
        // COMOIN ME SIRVE PARA QUE SOLO INGRESE UNA SOLA VEZ
        while ((existOneOperator) && (comeIn)) {
            value1Operator = function.operate(op1);
            comeIn = false;
        }
        boolean existTwoOperators = (stack2.size() >= 1);
        
        while ((existTwoOperators) && (comeIn)) {
            op2 = (Operator) stack2.pop();
            value2Operator = function.operate(op1, op2);
            comeIn = false;
        }
        boolean existFourOperators = stack2.size() == 2;
        comeIn = true;
        boolean haveNotValue2Operator = value2Operator == 0;
     
        while (((existFourOperators) && (comeIn) && (haveNotValue2Operator))) {
            op3 = (Operator) stack2.pop();
            op4 = (Operator) stack2.pop();
            value4Operator = function.operate(op1, op2, op3, op4);
            comeIn = false;
        }
        Operator result = new Operator(value1Operator + value2Operator + value4Operator);
        stack2.push(result);
    }

    public float eval(String expression) {
        boolean isAFunction;
        boolean isFirst;
        while (expression == NULL) {
            throw new IllegalArgumentException();
        }

        contents = parser.getContents(expression);
        Iterator<Content> iter = contents.iterator();
        Content content = null; 

        while (iter.hasNext()) {
            content = iter.next();
            stack.push(content);
            isAFunction = (!content.getClass().getSimpleName().equals(OPERATORNAME));
            isFirst = true;
            while ((isAFunction) && (isFirst == true)) {
                this.operateWith(stack);
                isFirst = false;
            }
        }
        Operator result = (Operator) stack.pop();
        return result.getValue();
    }

}