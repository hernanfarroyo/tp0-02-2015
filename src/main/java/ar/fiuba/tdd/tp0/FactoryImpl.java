package ar.fiuba.tdd.tp0;

import java.util.HashMap;

import java.util.Map;

public class FactoryImpl implements Factory<Content> {
    static Content NULL = null;
    Map<String, Content> reference;

    public FactoryImpl() {
        reference = new HashMap<String, Content>();
        reference.put("+", new Addition());
        reference.put("-", new Subtraction());
        reference.put("*", new Multiplication());
        reference.put("/", new Division());
        reference.put("MOD", new Mod());
        reference.put("++", new AdditionNArio());
        reference.put("--", new SustractionNArio());
        reference.put("**", new MultiplicationNArio());
        reference.put("//", new DivisionNArio());
        reference.put("MOD", new Mod());
    }

    public Content fabricate(String name) {
        Content content = reference.get(name);
        while (content != NULL) {
            return content;
        }
        return new Operator(Integer.parseInt(name));
    }

}