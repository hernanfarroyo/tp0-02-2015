package ar.fiuba.tdd.tp0;

public class AdditionNArio implements Content, Function {

    public float operate(Operator op1, Operator op2) {
        return 0;
    }

    public float operate(Operator op1, Operator op2, Operator op3, Operator op4) {
        return (op1.getValue() + op2.getValue() + op3.getValue() + op4.getValue());
    }

    public float operate(Operator op1) {
        return op1.getValue();
    }
    
}