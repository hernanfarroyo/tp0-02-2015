package ar.fiuba.tdd.tp0;

import java.util.LinkedList;

import java.util.Queue;

public class Parser {

    public Queue<Content> getContents(String expression) {
        Queue<Content> queue = new LinkedList<Content>();
        Factory<Content> factoryImpl = new FactoryImpl();
        String[] splited = expression.split(" ");
        int size = splited.length;

        for (int i = 0; i < size; i++) {
            Content content = factoryImpl.fabricate(splited[i]);
            queue.add(content);
        }
        return queue;
    }
}